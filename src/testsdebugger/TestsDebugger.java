/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testsdebugger;

import java.util.Scanner;

/**
 *
 * @author nachorod
 */
public class TestsDebugger {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String linea="";
        String nombre="";
        int contador;
        int numPalabras;
        int numCamas;
        String[] personas=new String[50];
        Scanner sc = new Scanner(System.in);
        int numCasos=Integer.parseInt(sc.nextLine());
        for (int i=1; i<=numCasos; i++) {
            contador=0;
            linea=sc.nextLine()+" ";
            nombre=extraeNombre(linea);
            linea=extraeLinea(linea);
            while (nombre!="F") {
                personas[contador]=nombre;
                contador++;
                nombre=extraeNombre(linea);
                linea=extraeLinea(linea);                
            }
            numCamas=Integer.parseInt(extraeNombre(linea));
            linea=extraeLinea(linea);
            numPalabras=Integer.parseInt(extraeNombre(linea));           
            linea=extraeLinea(linea);
            for (int j=0;j<contador;j++) {
                System.out.print(personas[j] + " ");
                System.out.println("");
            }
                System.out.println("");
                System.out.println("Camas: "+numCamas);
                System.out.println("Palabras: " +numPalabras);
                System.out.println("");
        }
        System.out.println("");

    }
    
    static String extraeNombre(String linea) {
        String palabra="";
        int pos=linea.indexOf(" ");
        palabra=linea.substring(0,pos);
        return palabra;
    }
    
    static String extraeLinea(String frase) {
        int pos=frase.indexOf(" ");
        String linea=frase.substring(pos+1);
        return linea;
    }    
}
